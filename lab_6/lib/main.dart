import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
            toolbarHeight: 50,
            title: const Text('BansosPeduli')),
        body: SingleChildScrollView(
            child: ConstrainedBox(
              constraints: BoxConstraints(),
              child: MyForm(),
            )
        )
      ),
    );
  }
}
// Create a Form widget.
class MyForm extends StatefulWidget {
  @override
  MyFormState createState() {
    return MyFormState();
  }
}
// Create a corresponding State class. This class holds data related to the form.
class MyFormState extends State<MyForm> {
  // Create a global key that uniquely identifies the Form widget
  // and allows validation of the form.
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    // Build a Form widget using the _formKey created above.
    return Form(
      key: _formKey,
      child: Container(
      padding: EdgeInsets.only(left:16.0,right: 16.0,top: 0),
      color: Color(0xFFffffff),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              margin: EdgeInsets.all(10),
              padding: EdgeInsets.all(10),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                color: Color(0xFF2196f3),
                borderRadius: BorderRadius.all(
                    Radius.circular(10.0)),
              ),
              child: Text("Citizen Registration",
                  style: TextStyle(
                  color: Color(0xffe6f3f4),
                  fontSize: 33,
                  wordSpacing: 2,
                ),
              )
            ),
            Text("\n  Telephone",
              style: TextStyle(
                color: Color(0xff2196f3),
                fontSize: 20,
                wordSpacing: 2,
              ),
            ),
            Container(
              margin: EdgeInsets.all(10),
              padding: EdgeInsets.all(15),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                color: Color(0xFFeeeeee),
                borderRadius: BorderRadius.all(
                    Radius.circular(10.0)),
              ),
            ),

            Text("  Password",
              style: TextStyle(
                color: Color(0xff2196f3),
                fontSize: 20,
                wordSpacing: 2,
              ),
            ),

            Container(
              margin: EdgeInsets.all(10),
              padding: EdgeInsets.all(15),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                color: Color(0xFFeeeeee),
                borderRadius: BorderRadius.all(
                    Radius.circular(10.0)),
              ),
            ),
            Text("  Name",
              style: TextStyle(
                color: Color(0xff2196f3),
                fontSize: 20,
                wordSpacing: 2,
              ),
            ),

            Container(
              margin: EdgeInsets.all(10),
              padding: EdgeInsets.all(15),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                color: Color(0xFFeeeeee),
                borderRadius: BorderRadius.all(
                    Radius.circular(10.0)),
              ),
            ),

            Text("  NIK",
              style: TextStyle(
                color: Color(0xff2196f3),
                fontSize: 20,
                wordSpacing: 2,
              ),
            ),

            Container(
              margin: EdgeInsets.all(10),
              padding: EdgeInsets.all(15),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                color: Color(0xFFeeeeee),
                borderRadius: BorderRadius.all(
                    Radius.circular(10.0)),
              ),
            ),

            Text("  SKTM Link",
              style: TextStyle(
                color: Color(0xff2196f3),
                fontSize: 20,
                wordSpacing: 2,
              ),
            ),

            Container(
              margin: EdgeInsets.all(10),
              padding: EdgeInsets.all(15),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                color: Color(0xFFeeeeee),
                borderRadius: BorderRadius.all(
                    Radius.circular(10.0)),
              ),
            ),

            Text("  Buku Tabungan Link",
              style: TextStyle(
                color: Color(0xff2196f3),
                fontSize: 20,
                wordSpacing: 2,
              ),
            ),

            Container(
              margin: EdgeInsets.all(10),
              padding: EdgeInsets.all(15),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                color: Color(0xFFeeeeee),
                borderRadius: BorderRadius.all(
                    Radius.circular(10.0)),
              ),
            ),

            Text("  Select Area:",
              style: TextStyle(
                color: Color(0xff2196f3),
                fontSize: 20,
                wordSpacing: 2,
              ),
            ),

            Container(
              margin: EdgeInsets.all(10),
              padding: EdgeInsets.all(15),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                color: Color(0xFFeeeeee),
                borderRadius: BorderRadius.all(
                    Radius.circular(10.0)),
              ),
            ),

            Container(
                margin: EdgeInsets.all(10),
                padding: EdgeInsets.only(left:10, right: 10,top: 10, bottom: 10),
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  color: Color(0xFF2196f3),
                  borderRadius: BorderRadius.all(
                      Radius.circular(10.0)),
                ),
                child: Text("Register",
                  style: TextStyle(
                    color: Color(0xffe6f3f4),
                    fontSize: 20,
                  ),
                )
            ),
          ],
        ),
      ),
    );
  }
}
