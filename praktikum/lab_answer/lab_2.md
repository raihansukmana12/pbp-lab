1. What is the difference between JSON and XML?

First we need to understand what actually is JSON, JSON stands for JavaScript Object Notation based on the JavaScript 
programming language and easy to understand and generate. It offers a human-readable collection of data that can be accessed logically.
it is a lightweight data-interchange format and it completely language independent. 

For the XML, Extensible Markup Language (XML) is a markup language that defines a set of rules for encoding documents in a format that is both human-readable and machine-readable. it was designed to carry data, not to display data. XML makes you able to define markup elements and generate customized markup language. An element is a basic unit in the XML language. The extension of XML file is .xml.

2. What is the difference between HTML and XML?

Hypertext Markup Language (HTML) is a programming language that displays data and describes a web page’s structure. Hypertext facilitates browsing the web by referring to the hyperlinks an HTML page contains. The hyperlink enables one to go to any place on the internet by clicking it. 

XML(eXtensible Markup Language) is also used to create web pages and web applications. It is dynamic because it is used to transport the data not for displaying the data. The design goals of XML focus on simplicity, generality, and usability across the Internet. It is a textual data format with strong support via Unicode for different human languages. Although the design of XML focuses on documents, the language is widely used for the representation of arbitrary data structures such as those used in web services.

Sources: 
https://www.geeksforgeeks.org/html-vs-xml/
https://www.guru99.com/xml-vs-html-difference.html
https://www.upgrad.com/blog/html-vs-xml/