from django.urls import path
from .views import index, friend_forms

urlpatterns = [
    path('', index, name='index'),
    path('add_friends', friend_forms, name='friend')
]
