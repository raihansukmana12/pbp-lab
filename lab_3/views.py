from django.shortcuts import render
from datetime import datetime, date
from lab_1.models import Friend
from .forms import FriendForm
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required

@login_required(login_url="/admin/login/")
def index(request):
    friends = Friend.objects.all()  
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)

@login_required(login_url="/admin/login/")
def friend_forms(request):
    if request.method == 'POST':
        form = FriendForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/lab-3')
    else:
        print(request.method)
        form = FriendForm()

    return render(request, 'lab3_form.html', {'form': form})
